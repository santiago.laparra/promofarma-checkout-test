# Promofarma Code Challenge  

## Description
### Promofarma Shopping Cart & Orders
- Promofarma is a marketplace that operates with different sellers that offer their products to be sold from Promofarma.
- Therefore, in our system we have to trace the selection of the product-seller once the user adds a product to the Shopping Cart and once the transaction ends.

### Requirements
- A product can be sold by one or several sellers. Every seller could have a different price.
- A product of a seller should have a name, net price, product type and vat value
- The product type values are bpc and otc 
- A purchase could contain several products
- A purchase has a shipping cost of 1,99€ that will be free if the total is over 20€
- The destination country is required in the purchase
- In a purchase, a product only should be sold by 1 seller
- A purchase couldn't be checked out (commit to buy) if the purchase has no products
- We should only sell otc products to France 
 

## Implement Shopping Cart API:
- Add a product to the purchase with the units desired (0 means remove the product).
- Get the purchase (products with the net and gross prices, units, sellers, etc.)
- Confirm purchase -> commit to buy.
- Send an email after the cart is confirmed (fake it)


## Nice to have
- Invariants, check business rules
- Asynchronous actions
- Check Idempotency
- Testing


## Example Workflow:
Elaborate a working set of example requests to fulfill a full buying process.  
Do not forget Readme with a working example and environment setup.
Take care along the developing process (commits).

## Delivery
Right here!!!
Estimated completion time: 1 day.
The implementation of the exercise is free.
The details of implementation and the approach taken will be evaluated.
It is not necessary to develop a graphical interface.
The test will be discussed together in the next online meeting
Enjoy Coding!
When you are done pls ping us here: <add email>
